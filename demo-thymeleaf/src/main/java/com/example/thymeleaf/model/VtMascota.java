package com.example.thymeleaf.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "VT_MASCOTA")
public class VtMascota implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_MASCOTA")
	private Long codMascota;
	@Column(name = "NOM_MASCOTA")
	private String nomMascota;
	@Column(name = "DES_TAMANIO")
	private String desTamanio;
	@Column(name = "DES_COLOR")
	private String desColor;

	public Long getCodMascota() {
		return codMascota;
	}

	public void setCodMascota(Long codMascota) {
		this.codMascota = codMascota;
	}

	public String getNomMascota() {
		return nomMascota;
	}

	public void setNomMascota(String nomMascota) {
		this.nomMascota = nomMascota;
	}

	public String getDesTamanio() {
		return desTamanio;
	}

	public void setDesTamanio(String desTamanio) {
		this.desTamanio = desTamanio;
	}

	public String getDesColor() {
		return desColor;
	}

	public void setDesColor(String desColor) {
		this.desColor = desColor;
	}

	@Override
	public String toString() {
		return "VtMascota [codMascota=" + codMascota + ", nomMascota=" + nomMascota + ", desTamanio=" + desTamanio
				+ ", desColor=" + desColor + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2958479467797629854L;

}
