package com.example.thymeleaf.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "VT_USUARIO")
public class VtUsuario implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_USUARIO")
	private long codUsuario;
	@Column(name = "DES_NOMBRE")
	private String desNombre;
	@Column(name = "DES_APELLIDO")
	private String desApellido;
	@Transient
	private List<VtDocumento> listDocumento;

	public long getCodUsuario() {
		return codUsuario;
	}

	public void setCodUsuario(long codUsuario) {
		this.codUsuario = codUsuario;
	}

	public String getDesNombre() {
		return desNombre;
	}

	public void setDesNombre(String desNombre) {
		this.desNombre = desNombre;
	}

	public String getDesApellido() {
		return desApellido;
	}

	public void setDesApellido(String desApellido) {
		this.desApellido = desApellido;
	}

	public List<VtDocumento> getListDocumento() {
		return listDocumento;
	}

	public void setListDocumento(List<VtDocumento> listDocumento) {
		this.listDocumento = listDocumento;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -7790853283125140474L;

}
