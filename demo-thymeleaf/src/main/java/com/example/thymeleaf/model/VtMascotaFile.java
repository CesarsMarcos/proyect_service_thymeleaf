package com.example.thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name = "VT_MACOTA_FILE")
public class VtMascotaFile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_MASCOTA_FILE")
	private long codMascotaFile;

	@Column(name = "COD_MASCOTA")
	private long codMascota;

	private String fileName;

	private String fileType;

	@Lob
	private byte[] data;

	public long getCodMascotaFile() {
		return codMascotaFile;
	}

	public void setCodMascotaFile(long codMascotaFile) {
		this.codMascotaFile = codMascotaFile;
	}

	public long getCodMascota() {
		return codMascota;
	}

	public void setCodMascota(long codMascota) {
		this.codMascota = codMascota;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public VtMascotaFile() {
		super();
	}

	public VtMascotaFile(long codMascota, String fileName, String fileType, byte[] data) {
		super();
		this.codMascota = codMascota;
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
	}

}
