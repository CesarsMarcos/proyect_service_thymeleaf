package com.example.thymeleaf.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "VT_CLIENTE")
public class VtCliente implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_CLIENTE")
	private Long codCliente;

	@Column(name = "NOM_CLIENTE")
	private String nomCliente;

	@Column(name = "DES_DIRECCION")
	private String desDireccion;

	@Column(name = "DES_EMAIL")
	private String desEmail;

	@Column(name = "TIENE_MASCOTAS")
	private Integer tieneMascotas;

	@Column(name = "IMAGEN")
	private String imagen;

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getNomCliente() {
		return nomCliente;
	}

	public void setNomCliente(String nomCliente) {
		this.nomCliente = nomCliente;
	}

	public String getDesDireccion() {
		return desDireccion;
	}

	public void setDesDireccion(String desDireccion) {
		this.desDireccion = desDireccion;
	}

	public String getDesEmail() {
		return desEmail;
	}

	public void setDesEmail(String desEmail) {
		this.desEmail = desEmail;
	}

	public Integer getTieneMascotas() {
		return tieneMascotas;
	}

	public void setTieneMascotas(Integer tieneMascotas) {
		this.tieneMascotas = tieneMascotas;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	@Override
	public String toString() {
		return "VtCliente [codCliente=" + codCliente + ", nomCliente=" + nomCliente + ", desDireccion=" + desDireccion
				+ ", desEmail=" + desEmail + ", tieneMascotas=" + tieneMascotas + "]";
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8729688699831404376L;

}
