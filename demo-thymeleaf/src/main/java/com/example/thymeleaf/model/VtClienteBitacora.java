package com.example.thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name = "VT_CLIENTE_BITACORA")
public class VtClienteBitacora {

	@Id
	@Column(name= "COD_CLIENTE_BITACORA")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codClienteBitacora;
	@Column (name="DESCRIPCION")
	private String descripcion;
	
	public Long getCodClienteBitacora() {
		return codClienteBitacora;
	}
	public void setCodClienteBitacora(Long codClienteBitacora) {
		this.codClienteBitacora = codClienteBitacora;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	
	
}
