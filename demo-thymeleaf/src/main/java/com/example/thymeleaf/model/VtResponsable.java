package com.example.thymeleaf.model;

import javax.persistence.*;

@Entity
@Table(name = "VT_RESPONSABLE")
public class VtResponsable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_RESPONSABLE")
	private Long codResponsable;
	@Column(name = "COD_ENTIDAD")
	private long codEntidad;
	@Column(name = "NOM_RESPONSABLE")
	private String nomResponsable;
	@Column(name = "APE_RESPONSABLE")
	private String apeResponsable;

	public Long getCodResponsable() {
		return codResponsable;
	}

	public void setCodResponsable(Long codResponsable) {
		this.codResponsable = codResponsable;
	}

	public long getCodEntidad() {
		return codEntidad;
	}

	public void setCodEntidad(long codEntidad) {
		this.codEntidad = codEntidad;
	}

	public String getNomResponsable() {
		return nomResponsable;
	}

	public void setNomResponsable(String nomResponsable) {
		this.nomResponsable = nomResponsable;
	}

	public String getApeResponsable() {
		return apeResponsable;
	}

	public void setApeResponsable(String apeResponsable) {
		this.apeResponsable = apeResponsable;
	}

}
