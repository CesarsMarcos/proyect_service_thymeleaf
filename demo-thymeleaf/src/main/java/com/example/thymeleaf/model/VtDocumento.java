package com.example.thymeleaf.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "VT_DOCUMENTO")
public class VtDocumento implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COD_DOCUMENTO")
	private long codDocumento;

	@Column(name = "COD_ENTIDAD")
	private long codEntidad;

	@Column(name = "comentario")
	private String comentario;

	@Column(name = "filename")
	private String filename;

	@Column(name = "content")
	@Lob
	private byte[] content;

	@Column(name = "content_type")
	private String contentType;

	@Column(name = "created")
	private Date created;



	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public long getCodDocumento() {
		return codDocumento;
	}

	public void setCodDocumento(long codDocumento) {
		this.codDocumento = codDocumento;
	}

	

	public long getCodEntidad() {
		return codEntidad;
	}

	public void setCodEntidad(long codEntidad) {
		this.codEntidad = codEntidad;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6534162625301956438L;

}
