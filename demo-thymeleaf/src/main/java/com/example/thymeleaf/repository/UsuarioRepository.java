package com.example.thymeleaf.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.thymeleaf.model.VtUsuario;

@Repository
public interface UsuarioRepository extends JpaRepository<VtUsuario, Serializable> {

	VtUsuario findByCodUsuario(long codUsuario);

	

}
