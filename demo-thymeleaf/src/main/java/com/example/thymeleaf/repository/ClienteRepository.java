package com.example.thymeleaf.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.thymeleaf.model.VtCliente;

@Repository
public interface ClienteRepository extends JpaRepository<VtCliente, Serializable> {

	VtCliente findByCodCliente(Long id);
		
}
