package com.example.thymeleaf.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.thymeleaf.model.VtDocumento;

@Repository
public interface DocumentoRepository extends JpaRepository<VtDocumento, Serializable> {

	VtDocumento findByCodDocumento(Long id);

	List<VtDocumento> findByCodEntidad(Long cod);

	
	
}
