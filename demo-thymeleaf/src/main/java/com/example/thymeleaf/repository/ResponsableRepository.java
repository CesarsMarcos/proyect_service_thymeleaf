package com.example.thymeleaf.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.thymeleaf.model.VtResponsable;

public interface ResponsableRepository extends JpaRepository<VtResponsable, Serializable> {

	List<VtResponsable> findByCodEntidad(Long cod);

}
