package com.example.thymeleaf.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.thymeleaf.model.VtMascotaFile;

@Repository
public interface MascotaFileRepository extends JpaRepository<VtMascotaFile, Serializable> {

	
}
