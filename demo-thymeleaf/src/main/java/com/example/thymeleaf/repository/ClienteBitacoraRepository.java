package com.example.thymeleaf.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.thymeleaf.model.VtClienteBitacora;

public interface ClienteBitacoraRepository extends JpaRepository<VtClienteBitacora, Serializable> {
	@Query("SELECT coalesce(max(usuario.codClienteBitacora), 0) FROM VtClienteBitacora usuario")
	Long getMaxId();
}
