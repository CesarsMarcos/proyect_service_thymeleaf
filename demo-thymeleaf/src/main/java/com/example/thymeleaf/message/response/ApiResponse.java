package com.example.thymeleaf.message.response;

public class ApiResponse {

	private Boolean success;
	private String messaje;
	private Object objeto;

	public ApiResponse(Boolean success, String messaje, Object objeto) {
		super();
		this.success = success;
		this.messaje = messaje;
		this.objeto = objeto;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessaje() {
		return messaje;
	}

	public void setMessaje(String messaje) {
		this.messaje = messaje;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

	
	

}
