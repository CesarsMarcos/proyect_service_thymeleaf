package com.example.thymeleaf.apicontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.thymeleaf.message.response.ApiResponse;
import com.example.thymeleaf.model.VtResponsable;
import com.example.thymeleaf.service.ResponsableServiceImpl;

@RestController
@RequestMapping("api/responsable/")
public class ApiResponsableController {

	@Autowired
	private ResponsableServiceImpl service;

	@GetMapping("listar-responsable/{cod}")
	public ResponseEntity<ApiResponse> ListarResponsable(@PathVariable ("cod") Long cod) {
		List<VtResponsable> listarResponsable = service.ListaResponsable(cod);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", listarResponsable), HttpStatus.OK);
	}

	@PostMapping("registro-responsable")
	public ResponseEntity<ApiResponse> RegistrarResponsable(@RequestBody VtResponsable responsable) {
		VtResponsable newResponsable = service.RegistroResponsable(responsable);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newResponsable), HttpStatus.OK);
	}

}
