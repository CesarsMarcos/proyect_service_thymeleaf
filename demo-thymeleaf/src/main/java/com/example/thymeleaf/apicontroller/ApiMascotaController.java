package com.example.thymeleaf.apicontroller;

import java.util.Arrays;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.thymeleaf.message.response.ApiResponse;
import com.example.thymeleaf.model.VtMascota;
import com.example.thymeleaf.model.VtMascotaFile;
import com.example.thymeleaf.service.MascotaFileServiceImpl;
import com.example.thymeleaf.service.MascotaServiceImpl;

@RestController
@RequestMapping("api/mascota/")
public class ApiMascotaController {

	private Logger LOG = LoggerFactory.getLogger(ApiMascotaController.class);

	@Autowired
	MascotaServiceImpl mascotaService;

	@Autowired
	MascotaFileServiceImpl mascotaFileService;

	@GetMapping("list-mascota")
	public ResponseEntity<ApiResponse> listarMascota() {
		LOG.info("[ApiMascotaController - listarMascota]");
		List<VtMascota> mascota = null;
		mascota = mascotaService.listMascota();
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", mascota), HttpStatus.OK);
	}

	@PostMapping("save-mascota")
	public ResponseEntity<ApiResponse> registrarMascota(VtMascota mascota) {
		VtMascota newMascota = mascotaService.registrarMascota(mascota);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newMascota), HttpStatus.OK);
	}

	@PostMapping("upload-file")
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
		VtMascotaFile dbFile = mascotaFileService.mascotaFile(file);
		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath().path("/downloadFile/")
				.path(dbFile.getFileType()).toUriString();
		// return new UploadFileResponse(dbFile.getFileName(),
		// fileDownloadUri,file.getContentType(), file.getSize());
		return new ResponseEntity<String>("Registrado con exito " + fileDownloadUri, HttpStatus.OK);
	}

	@PostMapping("upload-multifile")
	public ResponseEntity<?> uploadFile(@RequestParam("files") MultipartFile[] files) {
		return (ResponseEntity<?>) Arrays.asList(files).stream().map(file -> uploadFile(file))
				.collect(Collectors.toList());
	}

}
