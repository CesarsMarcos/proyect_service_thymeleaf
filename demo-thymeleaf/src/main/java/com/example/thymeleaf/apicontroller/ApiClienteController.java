package com.example.thymeleaf.apicontroller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.message.response.ApiResponse;
import com.example.thymeleaf.model.VtCliente;
import com.example.thymeleaf.repository.ClienteRepository;
import com.example.thymeleaf.service.ClienteServiceImpl;

@RestController
@RequestMapping("api/cliente/")
public class ApiClienteController {

	private Logger LOG = LoggerFactory.getLogger(ApiClienteController.class);

	@Autowired
	private ClienteServiceImpl clienteService;

	@Autowired
	private ClienteRepository clienteRepostory;

	@GetMapping("list-cliente")
	public ResponseEntity<ApiResponse> listMascota() {
		List<VtCliente> listaCliente = new ArrayList<VtCliente>();
		listaCliente = clienteService.listCliente();
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", listaCliente), HttpStatus.OK);
	}

	@PostMapping("registrar-cliente")
	public ResponseEntity<ApiResponse> saveMascota(@RequestBody VtCliente cliente) {
		VtCliente newCliente = null;
		if (cliente == null) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(true, "BAD_REQUEST", "Campos vacios"),
					HttpStatus.BAD_REQUEST);
		}
		try {

			newCliente = clienteService.registarCliente(cliente);
		} catch (Exception e) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "INTERNAL_SERVER_ERROR", e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newCliente), HttpStatus.OK);
	}

	@PostMapping("uploads")
	@Transactional
	private ResponseEntity<?> upload(@RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long id) {
		LOG.info("[CandidatoController - upload ]");
		Map<String, Object> response = new HashMap<>();
		VtCliente cliente = clienteRepostory.findByCodCliente(id);

		if (cliente == null) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "Cliente no existe", cliente),
					HttpStatus.BAD_REQUEST);
		}

		if (!archivo.isEmpty()) {
			String nombreArchivo = UUID.randomUUID().toString() + "_" + archivo.getOriginalFilename().replace(" ", "");
			Path rutaArchivo = Paths.get("D:\\uploads_img").resolve(nombreArchivo).toAbsolutePath();
			try {
				Files.copy(archivo.getInputStream(), rutaArchivo);
			} catch (IOException e) {
				response.put("mensaje", "Error al realizar la carga de la imagen " + nombreArchivo);
				response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}

			String nombreFotoAnterior = cliente.getImagen();
			if (nombreFotoAnterior != null && nombreFotoAnterior.length() > 0) {
				Path rutaFotoAnterior = Paths.get("D:\\uploads_img").resolve(nombreFotoAnterior).toAbsolutePath();
				File archivoFotoAnterior = rutaFotoAnterior.toFile();

				if (archivoFotoAnterior.exists() && archivoFotoAnterior.canRead()) {
					archivoFotoAnterior.delete();
				}
			}
			cliente.setCodCliente(id);
			cliente.setImagen(nombreArchivo);
			clienteRepostory.save(cliente);

			response.put("cliente", cliente);
			response.put("mensaje", "Imagen cargada con exito " + nombreArchivo);
		}
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@GetMapping("uploads/img/{nombreImagen:.+}")
	public ResponseEntity<Resource> verImagen(@PathVariable String nombreImagen) {
		Path rutaArchivo = Paths.get("D:\\uploads_img").resolve(nombreImagen).toAbsolutePath();
		Resource recurso = null;

		try {
			recurso = new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		if (!recurso.exists() && recurso.isReadable()) {
			throw new RuntimeException("No se pudo cargar la imagen " + nombreImagen);
		}

		HttpHeaders cabeceraHeaders = new HttpHeaders();
		cabeceraHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"");

		return new ResponseEntity<Resource>(recurso, cabeceraHeaders, HttpStatus.OK);

	}

}
