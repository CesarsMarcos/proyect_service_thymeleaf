package com.example.thymeleaf.apicontroller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.message.response.ApiResponse;
import com.example.thymeleaf.model.VtClienteBitacora;
import com.example.thymeleaf.model.VtDocumento;
import com.example.thymeleaf.model.VtUsuario;
import com.example.thymeleaf.repository.DocumentoRepository;
import com.example.thymeleaf.repository.UsuarioRepository;
import com.example.thymeleaf.service.ClienteBitacoraServiceImpl;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("api/usuario/")
public class ApiUsuarioController {

	private Logger LOG = LoggerFactory.getLogger(ApiUsuarioController.class);

	@Autowired
	private DocumentoRepository documentoRepostory;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private ClienteBitacoraServiceImpl clienteBitacoraService;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping("registrar-usuario")
	public ResponseEntity<?> registrarUsuario(@RequestParam("files") MultipartFile files,
			@RequestParam("usuario") String usuario) throws JsonParseException, JsonMappingException, IOException {

		VtUsuario usuarioRequest = mapper.readValue(usuario, VtUsuario.class);
		VtUsuario newUsuario = usuarioRepository.save(usuarioRequest);
		List<VtDocumento> listDoc = new ArrayList<>();
		VtDocumento docRequest = new VtDocumento();
		docRequest.setContent(files.getBytes());
		docRequest.setContentType(files.getContentType());
		docRequest.setFilename(files.getOriginalFilename());
		docRequest.setCreated(new Date());
		docRequest.setCodEntidad(newUsuario.getCodUsuario());

		VtDocumento newDocumento = documentoRepostory.save(docRequest);

		VtUsuario usuarioSave = usuarioRepository.findByCodUsuario(newUsuario.getCodUsuario());
		VtDocumento documentoSave = documentoRepostory.findByCodDocumento(newDocumento.getCodDocumento());
		listDoc.add(documentoSave);
		usuarioSave.setListDocumento(listDoc);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", usuarioSave), HttpStatus.OK);
	}

	@GetMapping("download/{codDocumentoFile}")
	public ResponseEntity<byte[]> verImagen(@PathVariable long codDocumentoFile) {
		VtDocumento documento = documentoRepostory.findByCodDocumento(codDocumentoFile);
		HttpHeaders cabeceraHeaders = new HttpHeaders();
		cabeceraHeaders.setContentLength(documento.getContent().length);
		cabeceraHeaders.add(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\"" + documento.getFilename() + "\"");
		return new ResponseEntity<byte[]>(documento.getContent(), cabeceraHeaders, HttpStatus.OK);
	}

	@PostMapping("registar-bitacora")
	public ResponseEntity<ApiResponse> RegistrarBitacora(@RequestBody VtClienteBitacora bitacora) {
		VtClienteBitacora newBitacora = clienteBitacoraService.RegistrarClienteBitacora(bitacora);
		Long ValorMaximo = clienteBitacoraService.codigoMaximo();
		LOG.info(Long.toString(ValorMaximo));

		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newBitacora), HttpStatus.OK);
	}

}
