package com.example.thymeleaf.apicontroller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.message.response.ApiResponse;
import com.example.thymeleaf.model.VtDocumento;
import com.example.thymeleaf.repository.DocumentoRepository;
import com.example.thymeleaf.service.DocumentoService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("api/documento/")
public class ApiDocumentoController {

	@Autowired
	private DocumentoService documentoService;

	@Autowired
	private DocumentoRepository documentoRepostory;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@PostMapping("upload-documento")
	public ResponseEntity<?> saveDocumento(@RequestParam("file") MultipartFile file, @RequestParam("id") Long id) {
		VtDocumento newDocumento = null;
		try {
			VtDocumento documento = documentoRepostory.findByCodDocumento(id);
			byte[] byteObjects = new byte[file.getBytes().length];
			int i = 0;
			for (byte b : file.getBytes()) {
				byteObjects[i++] = b;
			}
			documento.setContent(byteObjects);
			documento.setContentType(file.getContentType());

			documento.setFilename(file.getOriginalFilename());

			newDocumento = documentoService.registrarDocumento(documento);
		} catch (Exception e) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "ERROR", newDocumento),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newDocumento), HttpStatus.OK);
	}

	@PostMapping("uploadlist")
	public ResponseEntity<?> saveDocumentoList(@RequestParam("file") MultipartFile[] file,
			@RequestParam("id") Long id) {
		VtDocumento newDocumento = null;
		try {

		} catch (Exception e) {
			return new ResponseEntity<ApiResponse>(new ApiResponse(false, "ERROR", newDocumento),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newDocumento), HttpStatus.OK);
	}
	

	@PostMapping ("registrar-documento")
	public ResponseEntity<ApiResponse> registarDocumento(@RequestParam ("file") MultipartFile file,
			@RequestParam ("comentario") String comentario) throws JsonParseException, JsonMappingException, IOException{
		VtDocumento documentos = mapper.readValue(comentario,VtDocumento.class);
		documentos.setCodEntidad(1L);
		documentos.setContent(file.getBytes());
		documentos.setContentType(file.getContentType());
		documentos.setFilename(file.getOriginalFilename());
		documentos.setCreated(new Date());
		VtDocumento newDocumento= documentoRepostory.save(documentos);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", newDocumento), HttpStatus.OK);
	}
	
	
	@GetMapping("listar-documento/{cod}")
	public ResponseEntity<ApiResponse> listarDocumento(@PathVariable("cod") Long cod) {
		List<VtDocumento> listaDocumento = documentoRepostory.findByCodEntidad(cod);
		return new ResponseEntity<ApiResponse>(new ApiResponse(true, "OK", listaDocumento), HttpStatus.OK);
	}

	@GetMapping("download-documento/{codDocumentoFile}")
	public ResponseEntity<byte[]> DescargarDocumento(@PathVariable long codDocumentoFile) {
		System.out.println(codDocumentoFile);
		VtDocumento documento = documentoRepostory.findByCodDocumento(codDocumentoFile);
		HttpHeaders cabeceraHeaders = new HttpHeaders();
		cabeceraHeaders.setContentLength(documento.getContent().length);
		cabeceraHeaders.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + documento.getFilename() + "\"");
		return new ResponseEntity<byte[]>(documento.getContent(), cabeceraHeaders, HttpStatus.OK);
	}
	
	@GetMapping("download-documentos")
	public ResponseEntity<byte[]> DescargarDocumentos(@RequestBody List<Long> codigos,HttpServletResponse response) {
		List<byte[]> listaBytes = new ArrayList<byte[]>();
		HttpHeaders cabeceraHeaders = new HttpHeaders();
		VtDocumento documento= null;
		for(Long cod: codigos) {
			System.out.println("codigos "+cod);
			documento = documentoRepostory.findByCodDocumento(cod);
			listaBytes.add(documento.getContent());
			cabeceraHeaders.setContentLength(documento.getContent().length);
			cabeceraHeaders.add(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\"" + documento.getFilename() + "\"");
			return new ResponseEntity<byte[]>(documento.getContent(), cabeceraHeaders, HttpStatus.OK);
		}
		return null;
	}

}
