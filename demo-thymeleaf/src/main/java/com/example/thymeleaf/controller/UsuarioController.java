package com.example.thymeleaf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.thymeleaf.service.ClienteBitacoraServiceImpl;

@Controller
@RequestMapping("usuario-list")
public class UsuarioController {
	
	@Autowired
	private ClienteBitacoraServiceImpl bitacoraService;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public String  usuario (Model model) {
		model.addAttribute("codigoMaximo", bitacoraService.codigoMaximo());
		return "usuario";
	}
}
