package com.example.thymeleaf.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("mascota-list")
public class MascotaController {

	@RequestMapping(method= RequestMethod.GET)
	public String mascota() {
		return "mascota";
	}
	
}
