package com.example.thymeleaf.service;

import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.model.VtDocumento;

public interface DocumentoService {

	public VtDocumento registrarDocumento(VtDocumento documento);
	public VtDocumento registroDocumentoList(MultipartFile file);
}
