package com.example.thymeleaf.service;

import com.example.thymeleaf.model.VtClienteBitacora;

public interface ClienteBitacoraService {
	public VtClienteBitacora RegistrarClienteBitacora(VtClienteBitacora clienteBitacora);
	public Long codigoMaximo ();
}
