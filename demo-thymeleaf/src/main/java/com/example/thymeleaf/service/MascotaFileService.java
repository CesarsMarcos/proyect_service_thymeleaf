package com.example.thymeleaf.service;


import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.model.VtMascotaFile;

public interface MascotaFileService {

	public VtMascotaFile mascotaFile (MultipartFile file);
	
	
}
