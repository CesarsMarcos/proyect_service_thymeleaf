package com.example.thymeleaf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.thymeleaf.model.VtUsuario;
import com.example.thymeleaf.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public VtUsuario registrarUsuario(VtUsuario usuario) {
		VtUsuario newUsuario = usuarioRepository.save(usuario);
		return newUsuario;
	}

}
