package com.example.thymeleaf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.thymeleaf.model.VtCliente;
import com.example.thymeleaf.repository.ClienteRepository;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	ClienteRepository clienteRepository;

	@Override
	public VtCliente registarCliente(VtCliente cliente) {
		VtCliente newCliente = clienteRepository.save(cliente);
		return newCliente;
	}

	@Override
	public List<VtCliente> listCliente() {
		List<VtCliente> listCliente = clienteRepository.findAll();
		return listCliente;
	}


}
