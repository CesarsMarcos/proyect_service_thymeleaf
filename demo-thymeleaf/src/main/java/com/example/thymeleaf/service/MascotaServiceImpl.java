package com.example.thymeleaf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.thymeleaf.model.VtMascota;
import com.example.thymeleaf.repository.MascotaRepository;

@Service
public class MascotaServiceImpl implements MascotaService {

	@Autowired
	MascotaRepository mascotaRepository;

	@Override
	public VtMascota registrarMascota(VtMascota mascota) {
		VtMascota newMascota = mascotaRepository.save(mascota);
		return newMascota;
	}

	@Override
	public List<VtMascota> listMascota() {
		List<VtMascota> listMascota = mascotaRepository.findAll();
		return listMascota;
	}

}
