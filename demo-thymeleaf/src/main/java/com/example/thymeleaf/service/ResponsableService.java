package com.example.thymeleaf.service;

import java.util.List;

import com.example.thymeleaf.model.VtResponsable;

public interface ResponsableService {
	
	public VtResponsable RegistroResponsable (VtResponsable responsable); 
	public List<VtResponsable> ListaResponsable (Long cod);
	
}
