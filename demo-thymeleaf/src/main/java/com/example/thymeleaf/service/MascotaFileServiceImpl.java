package com.example.thymeleaf.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.model.VtMascotaFile;
import com.example.thymeleaf.repository.MascotaFileRepository;

@Service
public class MascotaFileServiceImpl implements MascotaFileService {

	@Autowired
	private MascotaFileRepository mascotaFileRepository;

	@Override
	public VtMascotaFile mascotaFile(MultipartFile file) {
		  // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        VtMascotaFile newMacotaFile = null;
        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
               // throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            VtMascotaFile dbFile = new VtMascotaFile(0, fileName, file.getContentType(), file.getBytes());
             newMacotaFile=   mascotaFileRepository.save(dbFile);  
        } catch (IOException ex) {
           // throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
        return newMacotaFile;	
	}
}
