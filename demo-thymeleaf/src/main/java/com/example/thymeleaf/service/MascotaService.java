package com.example.thymeleaf.service;

import java.util.List;

import com.example.thymeleaf.model.VtMascota;

public interface MascotaService {

	public VtMascota registrarMascota(VtMascota mascota);

	public List<VtMascota> listMascota();

}
