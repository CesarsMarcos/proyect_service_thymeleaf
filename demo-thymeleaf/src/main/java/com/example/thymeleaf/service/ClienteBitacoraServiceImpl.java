package com.example.thymeleaf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.example.thymeleaf.model.VtClienteBitacora;
import com.example.thymeleaf.repository.ClienteBitacoraRepository;

@Service
public class ClienteBitacoraServiceImpl  implements ClienteBitacoraService{

	@Autowired
	private ClienteBitacoraRepository repository;
	
	@Override
	public VtClienteBitacora RegistrarClienteBitacora(VtClienteBitacora clienteBitacora) {
		VtClienteBitacora newClienteBitacora = repository.save(clienteBitacora);
		return newClienteBitacora;
	}

	@Override
	@Bean
	public Long codigoMaximo() {
		Long codigo = repository.getMaxId();
		return codigo;
	}
	
	
		
}
