package com.example.thymeleaf.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.thymeleaf.model.VtResponsable;
import com.example.thymeleaf.repository.ResponsableRepository;
	
@Service
public class ResponsableServiceImpl implements ResponsableService{
	
	@Autowired
	private ResponsableRepository repository;

	@Override
	public VtResponsable RegistroResponsable(VtResponsable responsable) {
		VtResponsable newResponsable = repository.save(responsable);
		return newResponsable;
	}

	@Override
	public List<VtResponsable> ListaResponsable(Long cod) {
		List<VtResponsable> listResponsables = repository.findByCodEntidad(cod);
		return listResponsables;
	}

}
