package com.example.thymeleaf.service;

import java.util.List;

import com.example.thymeleaf.model.VtCliente;

public interface ClienteService {

	public VtCliente registarCliente (VtCliente cliente);
	public List<VtCliente> listCliente();
	
}	
