package com.example.thymeleaf.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.thymeleaf.model.VtDocumento;
import com.example.thymeleaf.repository.DocumentoRepository;

@Service
public class DocumentoServiceImpl implements DocumentoService {

	@Autowired
	private DocumentoRepository documentoRepository;

	@Override
	public VtDocumento registrarDocumento(VtDocumento documento) {
		VtDocumento documentoNew = documentoRepository.save(documento);
		return documentoNew;
	}

	@Override
	public VtDocumento registroDocumentoList(MultipartFile file) {
		
		VtDocumento newDocumento =null;
		
		byte[] byteDocumento;
		try {
			byteDocumento = new byte[file.getBytes().length];
			int i = 0;
			for (byte b : file.getBytes()) {
				byteDocumento[i++] = b;
			}
			
			VtDocumento documento= new VtDocumento();
			documento.setContentType(file.getContentType());
			documento.setFilename(file.getName());
			
			newDocumento = documentoRepository.save(documento);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return newDocumento;
	}

}
