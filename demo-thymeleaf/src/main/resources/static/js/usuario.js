$(document).ready(function() {
	bsCustomFileInput.init();
	listarDocumento();
	
	function conocerEvento(e) {
		if (!e) var e = window.event;
			document.getElementById("evento").value = e.type;
	}
});


$('.open-modal-usuario').on('click', function() {
	$('#modal-usuario').modal('show');
});

$('.open-modal-documentos').on('click', function() {
	$('#modal-documentos').modal('show');
	listarResponsable();
})

$('.open-modal-responsables').on('click', function() {
	listarResponsable();
	$('#modal-responsables').modal('show');
})

$('#btGuardarTodo').on('click', function() {
	let registro = new Object();
	registro.descripcion = $('#itDescripcionPrincipal').val();
	let myJSON = JSON.stringify(registro);
	$.ajax({
		url : 'api/usuario/registar-bitacora',
		type : 'POST',
		data : myJSON,
		dataType : 'json',
		contentType : 'application/json',
		success : function(data) {
			if (data.success) {
			}
		},
		error : function(data) {
			console.log();
		}
	});

});

$('#guardar-responsable').on('click', function() {
	let responsable = new Object();
	responsable.codEntidad = $('#itCodigo').val();
	responsable.nomResponsable = $('#itNombreUsuario').val();
	responsable.apeResponsable = $('#itApellidoUsuario').val();
	let myJson = JSON.stringify(responsable);

	$.ajax({
		url : 'api/responsable/registro-responsable',
		type : 'POST',
		data : myJson,
		dataType : 'json',
		contentType : 'application/json',
		success : function(data, textStatus) {
			listarResponsable();
		},
		error : function(data) {
			console.log(data);
		}
	});

})

function listarResponsable() {
	$('#tbResponsable tbody').html('');
	$.ajax({
		url : 'api/responsable/listar-responsable/' + $('#itCodigo').val(),
		type : 'GET',
		dataType : 'json',
		success : function(data, textStatus) {
			$.each(data.objeto, function(key, value) {
				$('#tbResponsable tbody').append(
						'<tr><th scope="row">' + value.codResponsable
								+ '</th><td>' + value.codEntidad
								+ '</td><td>' + value.nomResponsable
								+ '</td><td>' + value.apeResponsable
								+ '</td><td>@mdo</td>');
			});
		},
		error : function(data) {
			console.log(data);
		}
	});
}

function listarDocumento() {
	$('#tbDocumento tbody').html('');
	$.ajax({
		url : 'api/documento/listar-documento/' + $('#itCodigo').val(),
		type : 'GET',
		dataType : 'json',
		success : function(data, textStatus) {
			$.each(data.objeto, function(key, value) {
				$('#tbDocumento tbody').append(
						'<tr><th scope="row">' + value.codDocumento
								+ '</th><td>' + value.codEntidad
								+ '</td><td>' + value.filename
								+ '</td><td><input type="checkbox" name="documento" id="' + value.codDocumento
								+ '" class="form-check-input"></td>');
			});
		},
		error : function(data) {
			console.log(data);
		}
	});
}

$('#guardar-documentos').on('click', function() {
	// $(this).prop('disabled', true);
	let registro = new Object();
	registro.codEntidad = $('#itCodigo').val();
	registro.comentario = $('#itComentario').val();
	let myJSON = JSON.stringify(registro);
	// let file = $('#upload-documento')[0];
	let formData = new FormData();
	formData.append('file', $('input[name=documento]')[0].files[0]);
	formData.append('comentario', myJSON);

	$.ajax({
		type : 'POST',
		url : 'api/documento/registrar-documento',
		data : formData,
		encoding : "UTF-8",
		enctype : 'multipart/form-data',
		dataType : 'json',
		contentType : false,
		processData : false,
		contentType : false,
		cache : false,
		timeout : 600000,
		success : function(data) {
			if (data.success) {
				listarDocumento();
				// $(this).prop("disabled", false);
			} else {
				console.log(data);
				// $(this).prop("disabled", false);
			}
		},
		error : function(jqXHR, exception) {
			var msg = '';
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			// Swal.fire({type : 'info',title : 'Oops...',text : msg});
			console.log(msg);
		}
	});

});
/*
let documento = [];
$('#descargar-documentos').on('click', function(){
	documento=[];
	$('input[name="documento"]').each(function(index){
		if($(this).is(':checked')){
			documento.push($(this).attr('id'));
		}
	});
	documento.forEach(item =>{
		//$('#descargar-documentos').attr('href', 'api/documento/download-documento/'+item);
		window.location.href ='api/documento/download-documento/'+item;
	});

});
*/
let documento = [];
function descargarSeleccion(){
	
	documento=[];
	$('input[name="documento"]').each(function(index){
		if($(this).is(':checked')){
			documento.push($(this).attr('id'));
			 //location.href="api/documento/download-documento/"+$(this).attr('id');
		}
	});
	
	documento.forEach(item =>{
		setTimeout(function(){
	     location.href="api/documento/download-documento/"+item;
		},500 * item); 	
	});

};




$('#guardar-usuario').on('click', function() {
	$('#guardar-usuario').prop("disabled", true);
	var registro = new Object();
	registro.desNombre = $('#itNombreUsuario').val();
	registro.desApellido = $('#itApellidoUsuario').val();

	let myJSON = JSON.stringify(registro);
	// let file = $('#upload-form')[0].files[0];
	let formdata = new FormData();
	formdata.append('files', $('input[name=file]')[0].files[0])
	formdata.append('usuario', myJSON);
	$.ajax({
		type : 'POST',
		url : 'api/usuario/registrar-usuario',
		data : formdata,
		// encoding:"UTF-8",
		enctype : 'multipart/form-data',
		dataType : 'json',
		contentType : false,
		processData : false,
		contentType : false,
		cache : false,
		timeout : 600000,
		success : function(data) {
			if (data.success) {
				console.log(data);
				$('#upload-form')[0].reset();
				$("#guardar-usuario").prop("disabled", false);
			} else {
				console.log(data);
				$("#guardar-usuario").prop("disabled", false);
			}
		},
		error : function(jqXHR, exception) {
			var msg = '';
			if (jqXHR.status === 0) {
				msg = 'Not connect.\n Verify Network.';
			} else if (jqXHR.status == 404) {
				msg = 'Requested page not found. [404]';
			} else if (jqXHR.status == 500) {
				msg = 'Internal Server Error [500].';
			} else if (exception === 'parsererror') {
				msg = 'Requested JSON parse failed.';
			} else if (exception === 'timeout') {
				msg = 'Time out error.';
			} else if (exception === 'abort') {
				msg = 'Ajax request aborted.';
			} else {
				msg = 'Uncaught Error.\n' + jqXHR.responseText;
			}
			// Swal.fire({type : 'info',title : 'Oops...',text : msg});
			console.log(msg);
		}
	});

});
